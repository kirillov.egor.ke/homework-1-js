//     Считать с помощью модельного окна браузера данные пользователя: имя и возраст.
//     Если возраст меньше 18 лет - показать на экране сообщение: You are not allowed to visit this website.
//     Если возраст от 18 до 22 лет (включительно) - показать окно со следующим сообщением:
//     Are you sure you want to continue? и кнопками Ok, Cancel. Если пользователь нажал Ok, показать на экране сообщение: Welcome,  + имя пользователя.
//     Если пользователь нажал Cancel, показать на экране сообщение: You are not allowed to visit this website.
//     Если возраст больше 22 лет - показать на экране сообщение: Welcome,  + имя пользователя.
//     Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.


// Теория:
// Ответ на 2 вопроса: var - является глобальной переменной и не может относиться к конкретному блоку,
// что делает немного сложным (для разработчиков) современное написание кода. let и const современные переменные,
// которые являются блочними и упрощаю работу. Отличие в том, что const не может меняться при написании кода, а let может.

let userName = prompt('Как Вас зовут?');
let userAge = +prompt('Сколько Вам лет?');

userAge = Number(userAge);

if (userAge > 22) {
    alert(`Welcome, ${userName}`);
} else if (userAge < 18) {
    alert('You are not allowed to visit this website');
} else if (userAge >= 18 && userAge <= 22) {
    userAnswer = confirm('Are you sure you want to continue?');
    if (userAnswer === true) {
        alert(`Welcome, ${userName}`);
    } else {
        alert('You are not allowed to visit this website');
    }
}
